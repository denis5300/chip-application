package android.lab.mtuci.chipapplication.mvp.view

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndStrategy
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

@StateStrategyType(value = SkipStrategy::class)
interface IMainView : MvpView {

    fun requestDialog()

    @StateStrategyType(value = AddToEndStrategy::class)
    fun createChip(s: String)

}