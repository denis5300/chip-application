package android.lab.mtuci.chipapplication.mvp.presenter

import android.lab.mtuci.chipapplication.MainActivity
import android.lab.mtuci.chipapplication.R
import android.lab.mtuci.chipapplication.mvp.view.IMainView
import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter


@InjectViewState
class MainPresenter : MvpPresenter<IMainView>() {

    var text = ""
    var dialogIsShowing = false

    override fun attachView(view: IMainView?) {
        super.attachView(view)
        if (dialogIsShowing)
            viewState?.requestDialog()
    }

    private fun sendTextToChip() {
        viewState?.createChip(text)
        text = ""
        dialogIsShowing = false
    }

    fun showDialog(mainActivity: MainActivity) {
        //Создать билдер
        dialogIsShowing = true
        val builder = AlertDialog.Builder(mainActivity)
                .setView(R.layout.dialog_input)
                .setCancelable(false)
                .setNegativeButton("Cancel") { dialog, i -> dialog.dismiss(); dialogIsShowing = false}
                .setPositiveButton("OK")     { dialog, i -> dialog.dismiss(); sendTextToChip()}
        val dlg = builder.create()
        //Вывести
        dlg?.show()
        //Найти поле ввода
        dlg?.findViewById<EditText>(R.id.editTextChip)?.apply {
            //Установить текст если был
            setText(this@MainPresenter.text ?: "")
            //Поставить лисенер
            setTextChangeListener { this@MainPresenter.text = it }
        }
    }
    
    private fun EditText.setTextChangeListener(l:(changed: String) -> Unit ) {
        addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                l.invoke(p0.toString()) //Сохранить изменения в презентере
            }
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { }
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        })
    }

}