package android.lab.mtuci.chipapplication

import android.lab.mtuci.chipapplication.mvp.presenter.MainPresenter
import android.lab.mtuci.chipapplication.mvp.view.IMainView
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import com.arellomobile.mvp.MvpActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.PresenterType
import com.google.android.material.chip.Chip
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : MvpActivity(), IMainView {

    @InjectPresenter(type = PresenterType.GLOBAL)
    lateinit var mainPresenter: MainPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        addChipButton.setOnClickListener {
            requestDialog()
        }
    }

    override fun requestDialog() {
        mainPresenter.showDialog(this)
    }

    override fun createChip(s: String) {
        val ch = Chip(this)
        ch.text = s
        chipGroup.addView(ch)
    }

}
